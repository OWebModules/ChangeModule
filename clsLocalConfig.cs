﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OntologyAppDBConnector;
using ImportExport_Module;
using OntologyClasses.BaseClasses;
using System.Reflection;

namespace ChangeModule
{
    public class clsLocalConfig
{
    private const string cstrID_Ontology = "@GUID_Ontology@";
    private XMLImportWorker objImport;

    public Globals Globals { get; set; }

    private clsOntologyItem objOItem_DevConfig = new clsOntologyItem();
    public clsOntologyItem OItem_BaseConfig { get; set; }

    private OntologyModDBConnector objDBLevel_Config1;
    private OntologyModDBConnector objDBLevel_Config2;

    public clsOntologyItem OItem_attribute_datetime__to_do_list_ { get; set; }
    public clsOntologyItem OItem_attribute_datetimestamp { get; set; }
    public clsOntologyItem OItem_attribute_dbpostfix { get; set; }
    public clsOntologyItem OItem_attribute_description { get; set; }
    public clsOntologyItem OItem_attribute_enddate { get; set; }
    public clsOntologyItem OItem_attribute_id { get; set; }
    public clsOntologyItem OItem_attribute_message { get; set; }
    public clsOntologyItem OItem_attribute_standard { get; set; }
    public clsOntologyItem OItem_attribute_startdate { get; set; }
    public clsOntologyItem OItem_relationtype_belonging_done { get; set; }
    public clsOntologyItem OItem_relationtype_belonging_resource { get; set; }
    public clsOntologyItem OItem_relationtype_belonging_src1 { get; set; }
    public clsOntologyItem OItem_relationtype_belonging_src2 { get; set; }
    public clsOntologyItem OItem_relationtype_belonging_validity_period { get; set; }
    public clsOntologyItem OItem_relationtype_belongsto { get; set; }
    public clsOntologyItem OItem_relationtype_contained { get; set; }
    public clsOntologyItem OItem_relationtype_contains { get; set; }
    public clsOntologyItem OItem_relationtype_correlation_done { get; set; }
    public clsOntologyItem OItem_relationtype_error_queue { get; set; }
    public clsOntologyItem OItem_relationtype_finished_with { get; set; }
    public clsOntologyItem OItem_relationtype_isdescribedby { get; set; }
    public clsOntologyItem OItem_relationtype_isinstate { get; set; }
    public clsOntologyItem OItem_relationtype_last_done { get; set; }
    public clsOntologyItem OItem_relationtype_offered_by { get; set; }
    public clsOntologyItem OItem_relationtype_provides { get; set; }
    public clsOntologyItem OItem_relationtype_started_with { get; set; }
    public clsOntologyItem OItem_relationtype_superordinate { get; set; }
    public clsOntologyItem OItem_relationtype_time_measuring { get; set; }
    public clsOntologyItem OItem_relationtype_todo_for { get; set; }
    public clsOntologyItem OItem_relationtype_wascreatedby { get; set; }
    public clsOntologyItem OItem_token_logstate_create { get; set; }
    public clsOntologyItem OItem_token_logstate_dayend { get; set; }
    public clsOntologyItem OItem_token_logstate_daystart { get; set; }
    public clsOntologyItem OItem_token_logstate_error { get; set; }
    public clsOntologyItem OItem_token_logstate_finished { get; set; }
    public clsOntologyItem OItem_token_logstate_information { get; set; }
    public clsOntologyItem OItem_token_logstate_obsolete { get; set; }
    public clsOntologyItem OItem_token_logstate_solved { get; set; }
    public clsOntologyItem OItem_token_logstate_start { get; set; }
    public clsOntologyItem OItem_token_logstate_stop { get; set; }
    public clsOntologyItem OItem_token_process_incident { get; set; }
    public clsOntologyItem OItem_token_process_ticket_lists_all { get; set; }
    public clsOntologyItem OItem_token_process_ticket_lists_open { get; set; }
    public clsOntologyItem OItem_token_process_ticket_lists_processticketlist { get; set; }
    public clsOntologyItem OItem_token_process_ticket_lists_selected_date_range { get; set; }
    public clsOntologyItem OItem_token_process_ticket_lists_this_day { get; set; }
    public clsOntologyItem OItem_token_process_ticket_lists_this_month { get; set; }
    public clsOntologyItem OItem_token_process_ticket_lists_this_week { get; set; }
    public clsOntologyItem OItem_token_process_ticket_lists_this_year { get; set; }
    public clsOntologyItem OItem_type_correlated_process_ticket_creation { get; set; }
    public clsOntologyItem OItem_type_feiertage { get; set; }
    public clsOntologyItem OItem_type_group { get; set; }
    public clsOntologyItem OItem_type_incident { get; set; }
    public clsOntologyItem OItem_type_language { get; set; }
    public clsOntologyItem OItem_type_log { get; set; }
    public clsOntologyItem OItem_type_logentry { get; set; }
    public clsOntologyItem OItem_type_logstate { get; set; }
    public clsOntologyItem OItem_type_module { get; set; }
    public clsOntologyItem OItem_type_ort { get; set; }
    public clsOntologyItem OItem_type_process { get; set; }
    public clsOntologyItem OItem_type_process_last_done { get; set; }
    public clsOntologyItem OItem_type_process_log { get; set; }
    public clsOntologyItem OItem_type_process_ticket { get; set; }
    public clsOntologyItem OItem_type_process_ticket_lists { get; set; }
    public clsOntologyItem OItem_type_time_period { get; set; }
    public clsOntologyItem OItem_type_user { get; set; }
    public clsOntologyItem OItem_type_user_work_config { get; set; }
    public clsOntologyItem OItem_type_work_day { get; set; }



    private void get_Data_DevelopmentConfig()
    {
        var objORL_Ontology_To_OntolgyItems = new List<clsObjectRel> {new clsObjectRel {ID_Object = cstrID_Ontology,
                                                                                             ID_RelationType = Globals.RelationType_contains.GUID,
                                                                                             ID_Parent_Other = Globals.Class_OntologyItems.GUID}};

        var objOItem_Result = objDBLevel_Config1.GetDataObjectRel(objORL_Ontology_To_OntolgyItems, doIds: false);
        if (objOItem_Result.GUID == Globals.LState_Success.GUID)
        {
            if (objDBLevel_Config1.ObjectRels.Any())
            {

                objORL_Ontology_To_OntolgyItems = objDBLevel_Config1.ObjectRels.Select(oi => new clsObjectRel
                {
                    ID_Object = oi.ID_Other,
                    ID_RelationType = Globals.RelationType_belongingAttribute.GUID
                }).ToList();

                objORL_Ontology_To_OntolgyItems.AddRange(objDBLevel_Config1.ObjectRels.Select(oi => new clsObjectRel
                {
                    ID_Object = oi.ID_Other,
                    ID_RelationType = Globals.RelationType_belongingClass.GUID
                }));
                objORL_Ontology_To_OntolgyItems.AddRange(objDBLevel_Config1.ObjectRels.Select(oi => new clsObjectRel
                {
                    ID_Object = oi.ID_Other,
                    ID_RelationType = Globals.RelationType_belongingObject.GUID
                }));
                objORL_Ontology_To_OntolgyItems.AddRange(objDBLevel_Config1.ObjectRels.Select(oi => new clsObjectRel
                {
                    ID_Object = oi.ID_Other,
                    ID_RelationType = Globals.RelationType_belongingRelationType.GUID
                }));

                objOItem_Result = objDBLevel_Config2.GetDataObjectRel(objORL_Ontology_To_OntolgyItems, doIds: false);
                if (objOItem_Result.GUID == Globals.LState_Success.GUID)
                {
                    if (!objDBLevel_Config2.ObjectRels.Any())
                    {
                        throw new Exception("Config-Error");
                    }
                }
                else
                {
                    throw new Exception("Config-Error");
                }
            }
            else
            {
                throw new Exception("Config-Error");
            }

        }

    }

    public clsLocalConfig()
    {
        Globals = new Globals();
        set_DBConnection();
        get_Config();
    }

    public clsLocalConfig(Globals Globals)
    {
        this.Globals = Globals;
        set_DBConnection();
        get_Config();
    }

    private void set_DBConnection()
    {
        objDBLevel_Config1 = new OntologyModDBConnector(Globals);
        objDBLevel_Config2 = new OntologyModDBConnector(Globals);
        objImport = new XMLImportWorker(Globals);
    }

    private void get_Config()
    {
        try
        {
            get_Data_DevelopmentConfig();
            get_Config_AttributeTypes();
            get_Config_RelationTypes();
            get_Config_Classes();
            get_Config_Objects();
        }
        catch (Exception ex)
        {
            var objAssembly = Assembly.GetExecutingAssembly();
            AssemblyTitleAttribute[] objCustomAttributes = (AssemblyTitleAttribute[])objAssembly.GetCustomAttributes(typeof(AssemblyTitleAttribute), false);
            var strTitle = "Unbekannt";
            if (objCustomAttributes.Length == 1)
            {
                strTitle = objCustomAttributes.First().Title;
            }
            
            var objOItem_Result = objImport.ImportTemplates(objAssembly);
            if (objOItem_Result.GUID != Globals.LState_Error.GUID)
            {
                get_Data_DevelopmentConfig();
                get_Config_AttributeTypes();
                get_Config_RelationTypes();
                get_Config_Classes();
                get_Config_Objects();
            }
            else
            {
                throw new Exception("Config not importable");
            }
            
        }
    }

    private void get_Config_AttributeTypes()
    {
        var objOList_attribute_datetime__to_do_list_ = (from objOItem in objDBLevel_Config1.ObjectRels
                                                        where objOItem.ID_Object == cstrID_Ontology
                                                        join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                        where objRef.Name_Object.ToLower() == "attribute_datetime__to_do_list_".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                                        select objRef).ToList();

        if (objOList_attribute_datetime__to_do_list_.Any())
        {
            OItem_attribute_datetime__to_do_list_ = new clsOntologyItem()
            {
                GUID = objOList_attribute_datetime__to_do_list_.First().ID_Other,
                Name = objOList_attribute_datetime__to_do_list_.First().Name_Other,
                GUID_Parent = objOList_attribute_datetime__to_do_list_.First().ID_Parent_Other,
                Type = Globals.Type_AttributeType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_attribute_datetimestamp = (from objOItem in objDBLevel_Config1.ObjectRels
                                                where objOItem.ID_Object == cstrID_Ontology
                                                join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                where objRef.Name_Object.ToLower() == "attribute_datetimestamp".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                                select objRef).ToList();

        if (objOList_attribute_datetimestamp.Any())
        {
            OItem_attribute_datetimestamp = new clsOntologyItem()
            {
                GUID = objOList_attribute_datetimestamp.First().ID_Other,
                Name = objOList_attribute_datetimestamp.First().Name_Other,
                GUID_Parent = objOList_attribute_datetimestamp.First().ID_Parent_Other,
                Type = Globals.Type_AttributeType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_attribute_dbpostfix = (from objOItem in objDBLevel_Config1.ObjectRels
                                            where objOItem.ID_Object == cstrID_Ontology
                                            join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                            where objRef.Name_Object.ToLower() == "attribute_dbpostfix".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                            select objRef).ToList();

        if (objOList_attribute_dbpostfix.Any())
        {
            OItem_attribute_dbpostfix = new clsOntologyItem()
            {
                GUID = objOList_attribute_dbpostfix.First().ID_Other,
                Name = objOList_attribute_dbpostfix.First().Name_Other,
                GUID_Parent = objOList_attribute_dbpostfix.First().ID_Parent_Other,
                Type = Globals.Type_AttributeType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_attribute_description = (from objOItem in objDBLevel_Config1.ObjectRels
                                              where objOItem.ID_Object == cstrID_Ontology
                                              join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                              where objRef.Name_Object.ToLower() == "attribute_description".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                              select objRef).ToList();

        if (objOList_attribute_description.Any())
        {
            OItem_attribute_description = new clsOntologyItem()
            {
                GUID = objOList_attribute_description.First().ID_Other,
                Name = objOList_attribute_description.First().Name_Other,
                GUID_Parent = objOList_attribute_description.First().ID_Parent_Other,
                Type = Globals.Type_AttributeType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_attribute_enddate = (from objOItem in objDBLevel_Config1.ObjectRels
                                          where objOItem.ID_Object == cstrID_Ontology
                                          join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                          where objRef.Name_Object.ToLower() == "attribute_enddate".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                          select objRef).ToList();

        if (objOList_attribute_enddate.Any())
        {
            OItem_attribute_enddate = new clsOntologyItem()
            {
                GUID = objOList_attribute_enddate.First().ID_Other,
                Name = objOList_attribute_enddate.First().Name_Other,
                GUID_Parent = objOList_attribute_enddate.First().ID_Parent_Other,
                Type = Globals.Type_AttributeType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_attribute_id = (from objOItem in objDBLevel_Config1.ObjectRels
                                     where objOItem.ID_Object == cstrID_Ontology
                                     join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                     where objRef.Name_Object.ToLower() == "attribute_id".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                     select objRef).ToList();

        if (objOList_attribute_id.Any())
        {
            OItem_attribute_id = new clsOntologyItem()
            {
                GUID = objOList_attribute_id.First().ID_Other,
                Name = objOList_attribute_id.First().Name_Other,
                GUID_Parent = objOList_attribute_id.First().ID_Parent_Other,
                Type = Globals.Type_AttributeType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_attribute_message = (from objOItem in objDBLevel_Config1.ObjectRels
                                          where objOItem.ID_Object == cstrID_Ontology
                                          join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                          where objRef.Name_Object.ToLower() == "attribute_message".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                          select objRef).ToList();

        if (objOList_attribute_message.Any())
        {
            OItem_attribute_message = new clsOntologyItem()
            {
                GUID = objOList_attribute_message.First().ID_Other,
                Name = objOList_attribute_message.First().Name_Other,
                GUID_Parent = objOList_attribute_message.First().ID_Parent_Other,
                Type = Globals.Type_AttributeType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_attribute_standard = (from objOItem in objDBLevel_Config1.ObjectRels
                                           where objOItem.ID_Object == cstrID_Ontology
                                           join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                           where objRef.Name_Object.ToLower() == "attribute_standard".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                           select objRef).ToList();

        if (objOList_attribute_standard.Any())
        {
            OItem_attribute_standard = new clsOntologyItem()
            {
                GUID = objOList_attribute_standard.First().ID_Other,
                Name = objOList_attribute_standard.First().Name_Other,
                GUID_Parent = objOList_attribute_standard.First().ID_Parent_Other,
                Type = Globals.Type_AttributeType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_attribute_startdate = (from objOItem in objDBLevel_Config1.ObjectRels
                                            where objOItem.ID_Object == cstrID_Ontology
                                            join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                            where objRef.Name_Object.ToLower() == "attribute_startdate".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                            select objRef).ToList();

        if (objOList_attribute_startdate.Any())
        {
            OItem_attribute_startdate = new clsOntologyItem()
            {
                GUID = objOList_attribute_startdate.First().ID_Other,
                Name = objOList_attribute_startdate.First().Name_Other,
                GUID_Parent = objOList_attribute_startdate.First().ID_Parent_Other,
                Type = Globals.Type_AttributeType
            };
        }
        else
        {
            throw new Exception("config err");
        }


    }

    private void get_Config_RelationTypes()
    {
        var objOList_relationtype_belonging_done = (from objOItem in objDBLevel_Config1.ObjectRels
                                                    where objOItem.ID_Object == cstrID_Ontology
                                                    join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                    where objRef.Name_Object.ToLower() == "relationtype_belonging_done".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                    select objRef).ToList();

        if (objOList_relationtype_belonging_done.Any())
        {
            OItem_relationtype_belonging_done = new clsOntologyItem()
            {
                GUID = objOList_relationtype_belonging_done.First().ID_Other,
                Name = objOList_relationtype_belonging_done.First().Name_Other,
                GUID_Parent = objOList_relationtype_belonging_done.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_belonging_resource = (from objOItem in objDBLevel_Config1.ObjectRels
                                                        where objOItem.ID_Object == cstrID_Ontology
                                                        join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                        where objRef.Name_Object.ToLower() == "relationtype_belonging_resource".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                        select objRef).ToList();

        if (objOList_relationtype_belonging_resource.Any())
        {
            OItem_relationtype_belonging_resource = new clsOntologyItem()
            {
                GUID = objOList_relationtype_belonging_resource.First().ID_Other,
                Name = objOList_relationtype_belonging_resource.First().Name_Other,
                GUID_Parent = objOList_relationtype_belonging_resource.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_belonging_src1 = (from objOItem in objDBLevel_Config1.ObjectRels
                                                    where objOItem.ID_Object == cstrID_Ontology
                                                    join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                    where objRef.Name_Object.ToLower() == "relationtype_belonging_src1".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                    select objRef).ToList();

        if (objOList_relationtype_belonging_src1.Any())
        {
            OItem_relationtype_belonging_src1 = new clsOntologyItem()
            {
                GUID = objOList_relationtype_belonging_src1.First().ID_Other,
                Name = objOList_relationtype_belonging_src1.First().Name_Other,
                GUID_Parent = objOList_relationtype_belonging_src1.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_belonging_src2 = (from objOItem in objDBLevel_Config1.ObjectRels
                                                    where objOItem.ID_Object == cstrID_Ontology
                                                    join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                    where objRef.Name_Object.ToLower() == "relationtype_belonging_src2".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                    select objRef).ToList();

        if (objOList_relationtype_belonging_src2.Any())
        {
            OItem_relationtype_belonging_src2 = new clsOntologyItem()
            {
                GUID = objOList_relationtype_belonging_src2.First().ID_Other,
                Name = objOList_relationtype_belonging_src2.First().Name_Other,
                GUID_Parent = objOList_relationtype_belonging_src2.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_belonging_validity_period = (from objOItem in objDBLevel_Config1.ObjectRels
                                                               where objOItem.ID_Object == cstrID_Ontology
                                                               join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                               where objRef.Name_Object.ToLower() == "relationtype_belonging_validity_period".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                               select objRef).ToList();

        if (objOList_relationtype_belonging_validity_period.Any())
        {
            OItem_relationtype_belonging_validity_period = new clsOntologyItem()
            {
                GUID = objOList_relationtype_belonging_validity_period.First().ID_Other,
                Name = objOList_relationtype_belonging_validity_period.First().Name_Other,
                GUID_Parent = objOList_relationtype_belonging_validity_period.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_belongsto = (from objOItem in objDBLevel_Config1.ObjectRels
                                               where objOItem.ID_Object == cstrID_Ontology
                                               join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                               where objRef.Name_Object.ToLower() == "relationtype_belongsto".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                               select objRef).ToList();

        if (objOList_relationtype_belongsto.Any())
        {
            OItem_relationtype_belongsto = new clsOntologyItem()
            {
                GUID = objOList_relationtype_belongsto.First().ID_Other,
                Name = objOList_relationtype_belongsto.First().Name_Other,
                GUID_Parent = objOList_relationtype_belongsto.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_contained = (from objOItem in objDBLevel_Config1.ObjectRels
                                               where objOItem.ID_Object == cstrID_Ontology
                                               join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                               where objRef.Name_Object.ToLower() == "relationtype_contained".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                               select objRef).ToList();

        if (objOList_relationtype_contained.Any())
        {
            OItem_relationtype_contained = new clsOntologyItem()
            {
                GUID = objOList_relationtype_contained.First().ID_Other,
                Name = objOList_relationtype_contained.First().Name_Other,
                GUID_Parent = objOList_relationtype_contained.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_contains = (from objOItem in objDBLevel_Config1.ObjectRels
                                              where objOItem.ID_Object == cstrID_Ontology
                                              join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                              where objRef.Name_Object.ToLower() == "relationtype_contains".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                              select objRef).ToList();

        if (objOList_relationtype_contains.Any())
        {
            OItem_relationtype_contains = new clsOntologyItem()
            {
                GUID = objOList_relationtype_contains.First().ID_Other,
                Name = objOList_relationtype_contains.First().Name_Other,
                GUID_Parent = objOList_relationtype_contains.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_correlation_done = (from objOItem in objDBLevel_Config1.ObjectRels
                                                      where objOItem.ID_Object == cstrID_Ontology
                                                      join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                      where objRef.Name_Object.ToLower() == "relationtype_correlation_done".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                      select objRef).ToList();

        if (objOList_relationtype_correlation_done.Any())
        {
            OItem_relationtype_correlation_done = new clsOntologyItem()
            {
                GUID = objOList_relationtype_correlation_done.First().ID_Other,
                Name = objOList_relationtype_correlation_done.First().Name_Other,
                GUID_Parent = objOList_relationtype_correlation_done.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_error_queue = (from objOItem in objDBLevel_Config1.ObjectRels
                                                 where objOItem.ID_Object == cstrID_Ontology
                                                 join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                 where objRef.Name_Object.ToLower() == "relationtype_error_queue".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                 select objRef).ToList();

        if (objOList_relationtype_error_queue.Any())
        {
            OItem_relationtype_error_queue = new clsOntologyItem()
            {
                GUID = objOList_relationtype_error_queue.First().ID_Other,
                Name = objOList_relationtype_error_queue.First().Name_Other,
                GUID_Parent = objOList_relationtype_error_queue.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_finished_with = (from objOItem in objDBLevel_Config1.ObjectRels
                                                   where objOItem.ID_Object == cstrID_Ontology
                                                   join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                   where objRef.Name_Object.ToLower() == "relationtype_finished_with".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                   select objRef).ToList();

        if (objOList_relationtype_finished_with.Any())
        {
            OItem_relationtype_finished_with = new clsOntologyItem()
            {
                GUID = objOList_relationtype_finished_with.First().ID_Other,
                Name = objOList_relationtype_finished_with.First().Name_Other,
                GUID_Parent = objOList_relationtype_finished_with.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_isdescribedby = (from objOItem in objDBLevel_Config1.ObjectRels
                                                   where objOItem.ID_Object == cstrID_Ontology
                                                   join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                   where objRef.Name_Object.ToLower() == "relationtype_isdescribedby".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                   select objRef).ToList();

        if (objOList_relationtype_isdescribedby.Any())
        {
            OItem_relationtype_isdescribedby = new clsOntologyItem()
            {
                GUID = objOList_relationtype_isdescribedby.First().ID_Other,
                Name = objOList_relationtype_isdescribedby.First().Name_Other,
                GUID_Parent = objOList_relationtype_isdescribedby.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_isinstate = (from objOItem in objDBLevel_Config1.ObjectRels
                                               where objOItem.ID_Object == cstrID_Ontology
                                               join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                               where objRef.Name_Object.ToLower() == "relationtype_isinstate".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                               select objRef).ToList();

        if (objOList_relationtype_isinstate.Any())
        {
            OItem_relationtype_isinstate = new clsOntologyItem()
            {
                GUID = objOList_relationtype_isinstate.First().ID_Other,
                Name = objOList_relationtype_isinstate.First().Name_Other,
                GUID_Parent = objOList_relationtype_isinstate.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_last_done = (from objOItem in objDBLevel_Config1.ObjectRels
                                               where objOItem.ID_Object == cstrID_Ontology
                                               join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                               where objRef.Name_Object.ToLower() == "relationtype_last_done".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                               select objRef).ToList();

        if (objOList_relationtype_last_done.Any())
        {
            OItem_relationtype_last_done = new clsOntologyItem()
            {
                GUID = objOList_relationtype_last_done.First().ID_Other,
                Name = objOList_relationtype_last_done.First().Name_Other,
                GUID_Parent = objOList_relationtype_last_done.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_offered_by = (from objOItem in objDBLevel_Config1.ObjectRels
                                                where objOItem.ID_Object == cstrID_Ontology
                                                join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                where objRef.Name_Object.ToLower() == "relationtype_offered_by".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                select objRef).ToList();

        if (objOList_relationtype_offered_by.Any())
        {
            OItem_relationtype_offered_by = new clsOntologyItem()
            {
                GUID = objOList_relationtype_offered_by.First().ID_Other,
                Name = objOList_relationtype_offered_by.First().Name_Other,
                GUID_Parent = objOList_relationtype_offered_by.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_provides = (from objOItem in objDBLevel_Config1.ObjectRels
                                              where objOItem.ID_Object == cstrID_Ontology
                                              join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                              where objRef.Name_Object.ToLower() == "relationtype_provides".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                              select objRef).ToList();

        if (objOList_relationtype_provides.Any())
        {
            OItem_relationtype_provides = new clsOntologyItem()
            {
                GUID = objOList_relationtype_provides.First().ID_Other,
                Name = objOList_relationtype_provides.First().Name_Other,
                GUID_Parent = objOList_relationtype_provides.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_started_with = (from objOItem in objDBLevel_Config1.ObjectRels
                                                  where objOItem.ID_Object == cstrID_Ontology
                                                  join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                  where objRef.Name_Object.ToLower() == "relationtype_started_with".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                  select objRef).ToList();

        if (objOList_relationtype_started_with.Any())
        {
            OItem_relationtype_started_with = new clsOntologyItem()
            {
                GUID = objOList_relationtype_started_with.First().ID_Other,
                Name = objOList_relationtype_started_with.First().Name_Other,
                GUID_Parent = objOList_relationtype_started_with.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_superordinate = (from objOItem in objDBLevel_Config1.ObjectRels
                                                   where objOItem.ID_Object == cstrID_Ontology
                                                   join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                   where objRef.Name_Object.ToLower() == "relationtype_superordinate".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                   select objRef).ToList();

        if (objOList_relationtype_superordinate.Any())
        {
            OItem_relationtype_superordinate = new clsOntologyItem()
            {
                GUID = objOList_relationtype_superordinate.First().ID_Other,
                Name = objOList_relationtype_superordinate.First().Name_Other,
                GUID_Parent = objOList_relationtype_superordinate.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_time_measuring = (from objOItem in objDBLevel_Config1.ObjectRels
                                                    where objOItem.ID_Object == cstrID_Ontology
                                                    join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                    where objRef.Name_Object.ToLower() == "relationtype_time_measuring".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                    select objRef).ToList();

        if (objOList_relationtype_time_measuring.Any())
        {
            OItem_relationtype_time_measuring = new clsOntologyItem()
            {
                GUID = objOList_relationtype_time_measuring.First().ID_Other,
                Name = objOList_relationtype_time_measuring.First().Name_Other,
                GUID_Parent = objOList_relationtype_time_measuring.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_todo_for = (from objOItem in objDBLevel_Config1.ObjectRels
                                              where objOItem.ID_Object == cstrID_Ontology
                                              join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                              where objRef.Name_Object.ToLower() == "relationtype_todo_for".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                              select objRef).ToList();

        if (objOList_relationtype_todo_for.Any())
        {
            OItem_relationtype_todo_for = new clsOntologyItem()
            {
                GUID = objOList_relationtype_todo_for.First().ID_Other,
                Name = objOList_relationtype_todo_for.First().Name_Other,
                GUID_Parent = objOList_relationtype_todo_for.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_wascreatedby = (from objOItem in objDBLevel_Config1.ObjectRels
                                                  where objOItem.ID_Object == cstrID_Ontology
                                                  join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                  where objRef.Name_Object.ToLower() == "relationtype_wascreatedby".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                  select objRef).ToList();

        if (objOList_relationtype_wascreatedby.Any())
        {
            OItem_relationtype_wascreatedby = new clsOntologyItem()
            {
                GUID = objOList_relationtype_wascreatedby.First().ID_Other,
                Name = objOList_relationtype_wascreatedby.First().Name_Other,
                GUID_Parent = objOList_relationtype_wascreatedby.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }


    }

    private void get_Config_Objects()
    {
        var objOList_token_logstate_create = (from objOItem in objDBLevel_Config1.ObjectRels
                                              where objOItem.ID_Object == cstrID_Ontology
                                              join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                              where objRef.Name_Object.ToLower() == "token_logstate_create".ToLower() && objRef.Ontology == Globals.Type_Object
                                              select objRef).ToList();

        if (objOList_token_logstate_create.Any())
        {
            OItem_token_logstate_create = new clsOntologyItem()
            {
                GUID = objOList_token_logstate_create.First().ID_Other,
                Name = objOList_token_logstate_create.First().Name_Other,
                GUID_Parent = objOList_token_logstate_create.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_logstate_dayend = (from objOItem in objDBLevel_Config1.ObjectRels
                                              where objOItem.ID_Object == cstrID_Ontology
                                              join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                              where objRef.Name_Object.ToLower() == "token_logstate_dayend".ToLower() && objRef.Ontology == Globals.Type_Object
                                              select objRef).ToList();

        if (objOList_token_logstate_dayend.Any())
        {
            OItem_token_logstate_dayend = new clsOntologyItem()
            {
                GUID = objOList_token_logstate_dayend.First().ID_Other,
                Name = objOList_token_logstate_dayend.First().Name_Other,
                GUID_Parent = objOList_token_logstate_dayend.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_logstate_daystart = (from objOItem in objDBLevel_Config1.ObjectRels
                                                where objOItem.ID_Object == cstrID_Ontology
                                                join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                where objRef.Name_Object.ToLower() == "token_logstate_daystart".ToLower() && objRef.Ontology == Globals.Type_Object
                                                select objRef).ToList();

        if (objOList_token_logstate_daystart.Any())
        {
            OItem_token_logstate_daystart = new clsOntologyItem()
            {
                GUID = objOList_token_logstate_daystart.First().ID_Other,
                Name = objOList_token_logstate_daystart.First().Name_Other,
                GUID_Parent = objOList_token_logstate_daystart.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_logstate_error = (from objOItem in objDBLevel_Config1.ObjectRels
                                             where objOItem.ID_Object == cstrID_Ontology
                                             join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                             where objRef.Name_Object.ToLower() == "token_logstate_error".ToLower() && objRef.Ontology == Globals.Type_Object
                                             select objRef).ToList();

        if (objOList_token_logstate_error.Any())
        {
            OItem_token_logstate_error = new clsOntologyItem()
            {
                GUID = objOList_token_logstate_error.First().ID_Other,
                Name = objOList_token_logstate_error.First().Name_Other,
                GUID_Parent = objOList_token_logstate_error.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_logstate_finished = (from objOItem in objDBLevel_Config1.ObjectRels
                                                where objOItem.ID_Object == cstrID_Ontology
                                                join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                where objRef.Name_Object.ToLower() == "token_logstate_finished".ToLower() && objRef.Ontology == Globals.Type_Object
                                                select objRef).ToList();

        if (objOList_token_logstate_finished.Any())
        {
            OItem_token_logstate_finished = new clsOntologyItem()
            {
                GUID = objOList_token_logstate_finished.First().ID_Other,
                Name = objOList_token_logstate_finished.First().Name_Other,
                GUID_Parent = objOList_token_logstate_finished.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_logstate_information = (from objOItem in objDBLevel_Config1.ObjectRels
                                                   where objOItem.ID_Object == cstrID_Ontology
                                                   join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                   where objRef.Name_Object.ToLower() == "token_logstate_information".ToLower() && objRef.Ontology == Globals.Type_Object
                                                   select objRef).ToList();

        if (objOList_token_logstate_information.Any())
        {
            OItem_token_logstate_information = new clsOntologyItem()
            {
                GUID = objOList_token_logstate_information.First().ID_Other,
                Name = objOList_token_logstate_information.First().Name_Other,
                GUID_Parent = objOList_token_logstate_information.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_logstate_obsolete = (from objOItem in objDBLevel_Config1.ObjectRels
                                                where objOItem.ID_Object == cstrID_Ontology
                                                join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                where objRef.Name_Object.ToLower() == "token_logstate_obsolete".ToLower() && objRef.Ontology == Globals.Type_Object
                                                select objRef).ToList();

        if (objOList_token_logstate_obsolete.Any())
        {
            OItem_token_logstate_obsolete = new clsOntologyItem()
            {
                GUID = objOList_token_logstate_obsolete.First().ID_Other,
                Name = objOList_token_logstate_obsolete.First().Name_Other,
                GUID_Parent = objOList_token_logstate_obsolete.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_logstate_solved = (from objOItem in objDBLevel_Config1.ObjectRels
                                              where objOItem.ID_Object == cstrID_Ontology
                                              join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                              where objRef.Name_Object.ToLower() == "token_logstate_solved".ToLower() && objRef.Ontology == Globals.Type_Object
                                              select objRef).ToList();

        if (objOList_token_logstate_solved.Any())
        {
            OItem_token_logstate_solved = new clsOntologyItem()
            {
                GUID = objOList_token_logstate_solved.First().ID_Other,
                Name = objOList_token_logstate_solved.First().Name_Other,
                GUID_Parent = objOList_token_logstate_solved.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_logstate_start = (from objOItem in objDBLevel_Config1.ObjectRels
                                             where objOItem.ID_Object == cstrID_Ontology
                                             join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                             where objRef.Name_Object.ToLower() == "token_logstate_start".ToLower() && objRef.Ontology == Globals.Type_Object
                                             select objRef).ToList();

        if (objOList_token_logstate_start.Any())
        {
            OItem_token_logstate_start = new clsOntologyItem()
            {
                GUID = objOList_token_logstate_start.First().ID_Other,
                Name = objOList_token_logstate_start.First().Name_Other,
                GUID_Parent = objOList_token_logstate_start.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_logstate_stop = (from objOItem in objDBLevel_Config1.ObjectRels
                                            where objOItem.ID_Object == cstrID_Ontology
                                            join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                            where objRef.Name_Object.ToLower() == "token_logstate_stop".ToLower() && objRef.Ontology == Globals.Type_Object
                                            select objRef).ToList();

        if (objOList_token_logstate_stop.Any())
        {
            OItem_token_logstate_stop = new clsOntologyItem()
            {
                GUID = objOList_token_logstate_stop.First().ID_Other,
                Name = objOList_token_logstate_stop.First().Name_Other,
                GUID_Parent = objOList_token_logstate_stop.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_process_incident = (from objOItem in objDBLevel_Config1.ObjectRels
                                               where objOItem.ID_Object == cstrID_Ontology
                                               join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                               where objRef.Name_Object.ToLower() == "token_process_incident".ToLower() && objRef.Ontology == Globals.Type_Object
                                               select objRef).ToList();

        if (objOList_token_process_incident.Any())
        {
            OItem_token_process_incident = new clsOntologyItem()
            {
                GUID = objOList_token_process_incident.First().ID_Other,
                Name = objOList_token_process_incident.First().Name_Other,
                GUID_Parent = objOList_token_process_incident.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_process_ticket_lists_all = (from objOItem in objDBLevel_Config1.ObjectRels
                                                       where objOItem.ID_Object == cstrID_Ontology
                                                       join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                       where objRef.Name_Object.ToLower() == "token_process_ticket_lists_all".ToLower() && objRef.Ontology == Globals.Type_Object
                                                       select objRef).ToList();

        if (objOList_token_process_ticket_lists_all.Any())
        {
            OItem_token_process_ticket_lists_all = new clsOntologyItem()
            {
                GUID = objOList_token_process_ticket_lists_all.First().ID_Other,
                Name = objOList_token_process_ticket_lists_all.First().Name_Other,
                GUID_Parent = objOList_token_process_ticket_lists_all.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_process_ticket_lists_open = (from objOItem in objDBLevel_Config1.ObjectRels
                                                        where objOItem.ID_Object == cstrID_Ontology
                                                        join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                        where objRef.Name_Object.ToLower() == "token_process_ticket_lists_open".ToLower() && objRef.Ontology == Globals.Type_Object
                                                        select objRef).ToList();

        if (objOList_token_process_ticket_lists_open.Any())
        {
            OItem_token_process_ticket_lists_open = new clsOntologyItem()
            {
                GUID = objOList_token_process_ticket_lists_open.First().ID_Other,
                Name = objOList_token_process_ticket_lists_open.First().Name_Other,
                GUID_Parent = objOList_token_process_ticket_lists_open.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_process_ticket_lists_processticketlist = (from objOItem in objDBLevel_Config1.ObjectRels
                                                                     where objOItem.ID_Object == cstrID_Ontology
                                                                     join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                                     where objRef.Name_Object.ToLower() == "token_process_ticket_lists_processticketlist".ToLower() && objRef.Ontology == Globals.Type_Object
                                                                     select objRef).ToList();

        if (objOList_token_process_ticket_lists_processticketlist.Any())
        {
            OItem_token_process_ticket_lists_processticketlist = new clsOntologyItem()
            {
                GUID = objOList_token_process_ticket_lists_processticketlist.First().ID_Other,
                Name = objOList_token_process_ticket_lists_processticketlist.First().Name_Other,
                GUID_Parent = objOList_token_process_ticket_lists_processticketlist.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_process_ticket_lists_selected_date_range = (from objOItem in objDBLevel_Config1.ObjectRels
                                                                       where objOItem.ID_Object == cstrID_Ontology
                                                                       join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                                       where objRef.Name_Object.ToLower() == "token_process_ticket_lists_selected_date_range".ToLower() && objRef.Ontology == Globals.Type_Object
                                                                       select objRef).ToList();

        if (objOList_token_process_ticket_lists_selected_date_range.Any())
        {
            OItem_token_process_ticket_lists_selected_date_range = new clsOntologyItem()
            {
                GUID = objOList_token_process_ticket_lists_selected_date_range.First().ID_Other,
                Name = objOList_token_process_ticket_lists_selected_date_range.First().Name_Other,
                GUID_Parent = objOList_token_process_ticket_lists_selected_date_range.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_process_ticket_lists_this_day = (from objOItem in objDBLevel_Config1.ObjectRels
                                                            where objOItem.ID_Object == cstrID_Ontology
                                                            join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                            where objRef.Name_Object.ToLower() == "token_process_ticket_lists_this_day".ToLower() && objRef.Ontology == Globals.Type_Object
                                                            select objRef).ToList();

        if (objOList_token_process_ticket_lists_this_day.Any())
        {
            OItem_token_process_ticket_lists_this_day = new clsOntologyItem()
            {
                GUID = objOList_token_process_ticket_lists_this_day.First().ID_Other,
                Name = objOList_token_process_ticket_lists_this_day.First().Name_Other,
                GUID_Parent = objOList_token_process_ticket_lists_this_day.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_process_ticket_lists_this_month = (from objOItem in objDBLevel_Config1.ObjectRels
                                                              where objOItem.ID_Object == cstrID_Ontology
                                                              join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                              where objRef.Name_Object.ToLower() == "token_process_ticket_lists_this_month".ToLower() && objRef.Ontology == Globals.Type_Object
                                                              select objRef).ToList();

        if (objOList_token_process_ticket_lists_this_month.Any())
        {
            OItem_token_process_ticket_lists_this_month = new clsOntologyItem()
            {
                GUID = objOList_token_process_ticket_lists_this_month.First().ID_Other,
                Name = objOList_token_process_ticket_lists_this_month.First().Name_Other,
                GUID_Parent = objOList_token_process_ticket_lists_this_month.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_process_ticket_lists_this_week = (from objOItem in objDBLevel_Config1.ObjectRels
                                                             where objOItem.ID_Object == cstrID_Ontology
                                                             join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                             where objRef.Name_Object.ToLower() == "token_process_ticket_lists_this_week".ToLower() && objRef.Ontology == Globals.Type_Object
                                                             select objRef).ToList();

        if (objOList_token_process_ticket_lists_this_week.Any())
        {
            OItem_token_process_ticket_lists_this_week = new clsOntologyItem()
            {
                GUID = objOList_token_process_ticket_lists_this_week.First().ID_Other,
                Name = objOList_token_process_ticket_lists_this_week.First().Name_Other,
                GUID_Parent = objOList_token_process_ticket_lists_this_week.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_process_ticket_lists_this_year = (from objOItem in objDBLevel_Config1.ObjectRels
                                                             where objOItem.ID_Object == cstrID_Ontology
                                                             join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                             where objRef.Name_Object.ToLower() == "token_process_ticket_lists_this_year".ToLower() && objRef.Ontology == Globals.Type_Object
                                                             select objRef).ToList();

        if (objOList_token_process_ticket_lists_this_year.Any())
        {
            OItem_token_process_ticket_lists_this_year = new clsOntologyItem()
            {
                GUID = objOList_token_process_ticket_lists_this_year.First().ID_Other,
                Name = objOList_token_process_ticket_lists_this_year.First().Name_Other,
                GUID_Parent = objOList_token_process_ticket_lists_this_year.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }


    }

    private void get_Config_Classes()
    {
        var objOList_type_correlated_process_ticket_creation = (from objOItem in objDBLevel_Config1.ObjectRels
                                                                where objOItem.ID_Object == cstrID_Ontology
                                                                join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                                where objRef.Name_Object.ToLower() == "type_correlated_process_ticket_creation".ToLower() && objRef.Ontology == Globals.Type_Class
                                                                select objRef).ToList();

        if (objOList_type_correlated_process_ticket_creation.Any())
        {
            OItem_type_correlated_process_ticket_creation = new clsOntologyItem()
            {
                GUID = objOList_type_correlated_process_ticket_creation.First().ID_Other,
                Name = objOList_type_correlated_process_ticket_creation.First().Name_Other,
                GUID_Parent = objOList_type_correlated_process_ticket_creation.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_feiertage = (from objOItem in objDBLevel_Config1.ObjectRels
                                       where objOItem.ID_Object == cstrID_Ontology
                                       join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                       where objRef.Name_Object.ToLower() == "type_feiertage".ToLower() && objRef.Ontology == Globals.Type_Class
                                       select objRef).ToList();

        if (objOList_type_feiertage.Any())
        {
            OItem_type_feiertage = new clsOntologyItem()
            {
                GUID = objOList_type_feiertage.First().ID_Other,
                Name = objOList_type_feiertage.First().Name_Other,
                GUID_Parent = objOList_type_feiertage.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_group = (from objOItem in objDBLevel_Config1.ObjectRels
                                   where objOItem.ID_Object == cstrID_Ontology
                                   join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                   where objRef.Name_Object.ToLower() == "type_group".ToLower() && objRef.Ontology == Globals.Type_Class
                                   select objRef).ToList();

        if (objOList_type_group.Any())
        {
            OItem_type_group = new clsOntologyItem()
            {
                GUID = objOList_type_group.First().ID_Other,
                Name = objOList_type_group.First().Name_Other,
                GUID_Parent = objOList_type_group.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_incident = (from objOItem in objDBLevel_Config1.ObjectRels
                                      where objOItem.ID_Object == cstrID_Ontology
                                      join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                      where objRef.Name_Object.ToLower() == "type_incident".ToLower() && objRef.Ontology == Globals.Type_Class
                                      select objRef).ToList();

        if (objOList_type_incident.Any())
        {
            OItem_type_incident = new clsOntologyItem()
            {
                GUID = objOList_type_incident.First().ID_Other,
                Name = objOList_type_incident.First().Name_Other,
                GUID_Parent = objOList_type_incident.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_language = (from objOItem in objDBLevel_Config1.ObjectRels
                                      where objOItem.ID_Object == cstrID_Ontology
                                      join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                      where objRef.Name_Object.ToLower() == "type_language".ToLower() && objRef.Ontology == Globals.Type_Class
                                      select objRef).ToList();

        if (objOList_type_language.Any())
        {
            OItem_type_language = new clsOntologyItem()
            {
                GUID = objOList_type_language.First().ID_Other,
                Name = objOList_type_language.First().Name_Other,
                GUID_Parent = objOList_type_language.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_log = (from objOItem in objDBLevel_Config1.ObjectRels
                                 where objOItem.ID_Object == cstrID_Ontology
                                 join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                 where objRef.Name_Object.ToLower() == "type_log".ToLower() && objRef.Ontology == Globals.Type_Class
                                 select objRef).ToList();

        if (objOList_type_log.Any())
        {
            OItem_type_log = new clsOntologyItem()
            {
                GUID = objOList_type_log.First().ID_Other,
                Name = objOList_type_log.First().Name_Other,
                GUID_Parent = objOList_type_log.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_logentry = (from objOItem in objDBLevel_Config1.ObjectRels
                                      where objOItem.ID_Object == cstrID_Ontology
                                      join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                      where objRef.Name_Object.ToLower() == "type_logentry".ToLower() && objRef.Ontology == Globals.Type_Class
                                      select objRef).ToList();

        if (objOList_type_logentry.Any())
        {
            OItem_type_logentry = new clsOntologyItem()
            {
                GUID = objOList_type_logentry.First().ID_Other,
                Name = objOList_type_logentry.First().Name_Other,
                GUID_Parent = objOList_type_logentry.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_logstate = (from objOItem in objDBLevel_Config1.ObjectRels
                                      where objOItem.ID_Object == cstrID_Ontology
                                      join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                      where objRef.Name_Object.ToLower() == "type_logstate".ToLower() && objRef.Ontology == Globals.Type_Class
                                      select objRef).ToList();

        if (objOList_type_logstate.Any())
        {
            OItem_type_logstate = new clsOntologyItem()
            {
                GUID = objOList_type_logstate.First().ID_Other,
                Name = objOList_type_logstate.First().Name_Other,
                GUID_Parent = objOList_type_logstate.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_module = (from objOItem in objDBLevel_Config1.ObjectRels
                                    where objOItem.ID_Object == cstrID_Ontology
                                    join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                    where objRef.Name_Object.ToLower() == "type_module".ToLower() && objRef.Ontology == Globals.Type_Class
                                    select objRef).ToList();

        if (objOList_type_module.Any())
        {
            OItem_type_module = new clsOntologyItem()
            {
                GUID = objOList_type_module.First().ID_Other,
                Name = objOList_type_module.First().Name_Other,
                GUID_Parent = objOList_type_module.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_ort = (from objOItem in objDBLevel_Config1.ObjectRels
                                 where objOItem.ID_Object == cstrID_Ontology
                                 join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                 where objRef.Name_Object.ToLower() == "type_ort".ToLower() && objRef.Ontology == Globals.Type_Class
                                 select objRef).ToList();

        if (objOList_type_ort.Any())
        {
            OItem_type_ort = new clsOntologyItem()
            {
                GUID = objOList_type_ort.First().ID_Other,
                Name = objOList_type_ort.First().Name_Other,
                GUID_Parent = objOList_type_ort.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_process = (from objOItem in objDBLevel_Config1.ObjectRels
                                     where objOItem.ID_Object == cstrID_Ontology
                                     join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                     where objRef.Name_Object.ToLower() == "type_process".ToLower() && objRef.Ontology == Globals.Type_Class
                                     select objRef).ToList();

        if (objOList_type_process.Any())
        {
            OItem_type_process = new clsOntologyItem()
            {
                GUID = objOList_type_process.First().ID_Other,
                Name = objOList_type_process.First().Name_Other,
                GUID_Parent = objOList_type_process.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_process_last_done = (from objOItem in objDBLevel_Config1.ObjectRels
                                               where objOItem.ID_Object == cstrID_Ontology
                                               join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                               where objRef.Name_Object.ToLower() == "type_process_last_done".ToLower() && objRef.Ontology == Globals.Type_Class
                                               select objRef).ToList();

        if (objOList_type_process_last_done.Any())
        {
            OItem_type_process_last_done = new clsOntologyItem()
            {
                GUID = objOList_type_process_last_done.First().ID_Other,
                Name = objOList_type_process_last_done.First().Name_Other,
                GUID_Parent = objOList_type_process_last_done.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_process_log = (from objOItem in objDBLevel_Config1.ObjectRels
                                         where objOItem.ID_Object == cstrID_Ontology
                                         join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                         where objRef.Name_Object.ToLower() == "type_process_log".ToLower() && objRef.Ontology == Globals.Type_Class
                                         select objRef).ToList();

        if (objOList_type_process_log.Any())
        {
            OItem_type_process_log = new clsOntologyItem()
            {
                GUID = objOList_type_process_log.First().ID_Other,
                Name = objOList_type_process_log.First().Name_Other,
                GUID_Parent = objOList_type_process_log.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_process_ticket = (from objOItem in objDBLevel_Config1.ObjectRels
                                            where objOItem.ID_Object == cstrID_Ontology
                                            join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                            where objRef.Name_Object.ToLower() == "type_process_ticket".ToLower() && objRef.Ontology == Globals.Type_Class
                                            select objRef).ToList();

        if (objOList_type_process_ticket.Any())
        {
            OItem_type_process_ticket = new clsOntologyItem()
            {
                GUID = objOList_type_process_ticket.First().ID_Other,
                Name = objOList_type_process_ticket.First().Name_Other,
                GUID_Parent = objOList_type_process_ticket.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_process_ticket_lists = (from objOItem in objDBLevel_Config1.ObjectRels
                                                  where objOItem.ID_Object == cstrID_Ontology
                                                  join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                  where objRef.Name_Object.ToLower() == "type_process_ticket_lists".ToLower() && objRef.Ontology == Globals.Type_Class
                                                  select objRef).ToList();

        if (objOList_type_process_ticket_lists.Any())
        {
            OItem_type_process_ticket_lists = new clsOntologyItem()
            {
                GUID = objOList_type_process_ticket_lists.First().ID_Other,
                Name = objOList_type_process_ticket_lists.First().Name_Other,
                GUID_Parent = objOList_type_process_ticket_lists.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_time_period = (from objOItem in objDBLevel_Config1.ObjectRels
                                         where objOItem.ID_Object == cstrID_Ontology
                                         join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                         where objRef.Name_Object.ToLower() == "type_time_period".ToLower() && objRef.Ontology == Globals.Type_Class
                                         select objRef).ToList();

        if (objOList_type_time_period.Any())
        {
            OItem_type_time_period = new clsOntologyItem()
            {
                GUID = objOList_type_time_period.First().ID_Other,
                Name = objOList_type_time_period.First().Name_Other,
                GUID_Parent = objOList_type_time_period.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_user = (from objOItem in objDBLevel_Config1.ObjectRels
                                  where objOItem.ID_Object == cstrID_Ontology
                                  join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                  where objRef.Name_Object.ToLower() == "type_user".ToLower() && objRef.Ontology == Globals.Type_Class
                                  select objRef).ToList();

        if (objOList_type_user.Any())
        {
            OItem_type_user = new clsOntologyItem()
            {
                GUID = objOList_type_user.First().ID_Other,
                Name = objOList_type_user.First().Name_Other,
                GUID_Parent = objOList_type_user.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_user_work_config = (from objOItem in objDBLevel_Config1.ObjectRels
                                              where objOItem.ID_Object == cstrID_Ontology
                                              join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                              where objRef.Name_Object.ToLower() == "type_user_work_config".ToLower() && objRef.Ontology == Globals.Type_Class
                                              select objRef).ToList();

        if (objOList_type_user_work_config.Any())
        {
            OItem_type_user_work_config = new clsOntologyItem()
            {
                GUID = objOList_type_user_work_config.First().ID_Other,
                Name = objOList_type_user_work_config.First().Name_Other,
                GUID_Parent = objOList_type_user_work_config.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_work_day = (from objOItem in objDBLevel_Config1.ObjectRels
                                      where objOItem.ID_Object == cstrID_Ontology
                                      join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                      where objRef.Name_Object.ToLower() == "type_work_day".ToLower() && objRef.Ontology == Globals.Type_Class
                                      select objRef).ToList();

        if (objOList_type_work_day.Any())
        {
            OItem_type_work_day = new clsOntologyItem()
            {
                GUID = objOList_type_work_day.First().ID_Other,
                Name = objOList_type_work_day.First().Name_Other,
                GUID_Parent = objOList_type_work_day.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }


    }
}

}