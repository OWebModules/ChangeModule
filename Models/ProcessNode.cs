﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChangeModule.Models
{
    public class ProcessNode
    {
        public string IdProcessNode { get; set; }
        public string NameProcessNode { get; set; }

    }
}
